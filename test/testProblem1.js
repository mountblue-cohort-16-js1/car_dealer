const inventory = require('../inventory.js');
const tester = require('../problem1.js');

let result = tester(inventory, 33);

if (result.length !== 0) {
    console.log("Car " + result.id + " is a " + result.car_year + " " + result.car_make + " " + result.car_model);
}
