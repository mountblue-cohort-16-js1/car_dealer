const sortedCarModels = function(inventory) {
    if(!!inventory && typeof inventory === 'object' && inventory.length !== 0) {
        let sortedCarArray = [];
        for (let i=0; i<inventory.length; i++) {
            let modelName = inventory[i].car_model
            sortedCarArray.push(modelName.charAt(0).toUpperCase() + modelName.slice(1));
            }
        return sortedCarArray.sort();
        }
    return [];
    }

module.exports = sortedCarModels;
