const inventory = require('./inventory.js');
const yearFunction = require('./problem4.js');
let yearArray = yearFunction(inventory);

const carOlderThan = function(inventory, yearOpt) {
    if(!!inventory && typeof inventory === 'object' && inventory.length !== 0 && !!yearOpt) {
        let olderCars = [];
        for (let i=0; i<yearArray.length; i++) {
            if (yearArray[i] < yearOpt) {
                car = inventory[i].car_make + ' ' + inventory[i].car_model;
                olderCars.push(car);
                }        
            }
        let carInfo = [];
        carInfo.push(olderCars, 'Total ' + olderCars.length + ' car(s) were made before ' + yearOpt); 
        return carInfo;
        }
    return [];
    }

module.exports = carOlderThan;
