const onlyBmwAudi = function(inventory) {
    if(!!inventory && typeof inventory === 'object' && inventory.length !== 0) {
        let specialArray = [];
        for (let i=0; i<inventory.length; i++) {
            if (inventory[i].car_make === 'BMW' || inventory[i].car_make === 'Audi') {
                car = inventory[i].car_year + ' ' + inventory[i].car_make + ' ' + inventory[i].car_model;
                specialArray.push(car);
                }
            }
        return specialArray;
        }
    return [];
    }

module.exports = onlyBmwAudi;
