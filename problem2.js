const lastCar = function(inventory) {
    if(!!inventory && typeof inventory === 'object' && inventory.length !== 0) {
        const carInfo = inventory[inventory.length-1];
        return carInfo;
        }
    return [];
    }

module.exports = lastCar;
