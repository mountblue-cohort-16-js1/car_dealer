const carYears = function(inventory) {
    if(!!inventory && typeof inventory === 'object' && inventory.length !== 0) {
        let yearArray = [];
        for (let i=0; i<inventory.length; i++) {
            yearArray.push(inventory[i].car_year);
            }
        return yearArray;
        }
    return [];
    }

module.exports = carYears;
