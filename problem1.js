const carId = function(inventory, id) {
    if(!!inventory && !!id && typeof inventory === 'object' && inventory.length !== 0) {
        for (let i = 0; i<inventory.length; i++) {
            if (inventory[i].id === id) {
                const carInfo = inventory[i];
                return carInfo;
            }
        }
    }
    return [];
}

module.exports = carId;
